import java.util.ArrayList;

public class Book {
    private String name;
    private ArrayList<String> authors;
    private BookGenre genre;
    private String ISBN;

    public Book(String name, ArrayList<String> authors, BookGenre genre, String ISBN) {
        this.name = name;
        this.authors = authors;
        this.genre = genre;
        this.ISBN = ISBN;
    }

    //  - books (they have title, author(s), genre (eg like Fantasy
    //  , Self-Help, Science), and a unique identifier:
    //  ISBN (International Standard Book Number))

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList <String> authors) {
        this.authors = authors;
    }

    public BookGenre getGenre() {
        return genre;
    }

    public void setGenre(BookGenre genre) {
        this.genre = genre;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }
}
