import java.util.ArrayList;

public class ScientificArticle {
    // - scientific articles (they also have title, author(s),
    // a theme (eg: Computer Science, Biology, Psychology)
    // and a unique identifier DOI)

    private String title;
    private ArrayList<String> authors;
    private Theme theme;
    private String DOI;



    public ScientificArticle(String title, ArrayList authors, Theme theme, String DOI) {
        this.title = title;
        this.authors = authors;
        this.theme = theme;
        this.DOI = DOI;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList getAuthors() {
        return authors;
    }

    public void setAuthors(ArrayList authors) {
        this.authors = authors;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public String getDOI() {
        return DOI;
    }

    public void setDOI(String DOI) {
        this.DOI = DOI;
    }


}
