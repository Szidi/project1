import java.io.*;
import java.util.ArrayList;

public class Test {
    public static void main(String[] args) throws java.io.IOException {

        BufferedReader reader = new BufferedReader(new FileReader("read.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("write.txt"));

        ArrayList<Book> books = new ArrayList<>();
        ArrayList<Movie> movies = new ArrayList<>();
        ArrayList<ScientificArticle> scientificArticles = new ArrayList<>();
        ArrayList<String> authorsList = new ArrayList<>();
        ArrayList<String> actorsList = new ArrayList<>();
        String line;


        while ((line = reader.readLine()) != null) {
            String[] infos = line.split("/");

            switch (infos[0]) {
                case "B": {

                    String name = infos[1];
                    while ((line = reader.readLine()) != null) {
                        String[] authors = line.split(";");

                        int i = 0;
                        while (authors[i] != null) {
                            authorsList.add(authors[i]);
                            i++;
                        }
                    }
                    BookGenre genre = BookGenre.valueOf(infos[3]);
                    String ISBN = infos[4];
                    Book book = new Book(name, authorsList, genre, ISBN);
                    books.add(book);

                    break;
                }
                case "M": {
                    String title = infos[1];
                    MovieGenre genre = MovieGenre.valueOf(infos[3]);
                    while ((line = reader.readLine()) != null) {
                        String[] actors = line.split(";");
                        int i = 0;

                        while (actors[i] != null) {
                            actorsList.add(actors[i]);
                            i++;
                        }
                    }
                    String IMDBid = infos[4];
                    Movie movie = new Movie(title, genre, actorsList, IMDBid);
                    movies.add(movie);

                    break;
                }
                case "S": {
                    String title = infos[1];
                    while ((line = reader.readLine()) != null) {
                        String[] authors = line.split(";");
                        int i = 0;

                        while (authors[i] != null) {
                            authorsList.add(authors[i]);
                            i++;
                        }
                    }
                    Theme theme = Theme.valueOf(infos[3]);
                    String DOI = infos[4];

                    ScientificArticle scientificArticle = new ScientificArticle(title, authorsList, theme, DOI);
                    scientificArticles.add(scientificArticle);
                    break;
                }
            }


        }

        writer.close();
        reader.close();

        System.out.println(books);
        System.out.println(movies);
        System.out.println(scientificArticles);

    }
}