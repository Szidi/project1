import java.util.ArrayList;

public class Movie {
    // - movies ( they have title and genre, a list of
    // participating actors, and they can be identified
    // via an IMDB id)

    private String title;
    private MovieGenre movieGenre;
    private ArrayList <String> actors;
    private String IMDBid;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public MovieGenre getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(MovieGenre movieGenre) {
        this.movieGenre = movieGenre;
    }

    public ArrayList<String> getActors() {
        return actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public String getIMDBid() {
        return IMDBid;
    }

    public void setIMDBid(String IMDBid) {
        this.IMDBid = IMDBid;
    }



    public Movie(String title, MovieGenre movieGenre, ArrayList<String> actors, String IMDBid) {
        this.title = title;
        this.movieGenre = movieGenre;
        this.actors = actors;
        this.IMDBid = IMDBid;
    }



}
